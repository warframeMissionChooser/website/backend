use super::ItemId;
use serde::{Deserialize, Serialize};
use warframe_shared::Polarity;

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "PascalCase")]
pub struct Suit {
    /// Name
    pub item_type: String,

    /// XP Value
    #[serde(rename = "XP")]
    #[serde(default)]
    pub xp: i32,

    // pub configs: Vec<Config>,
    // pub upgrade_ver: Option<i64>,
    // pub infestation_date: Option<InfestationDate>,

    // pub features: Option<i64>,
    pub item_id: ItemId,

    pub focus_lens: Option<String>,

    #[serde(default)]
    pub polarity: Vec<LocatedPolarity>,

    #[serde(default)]
    pub polarized: i8,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "PascalCase")]
pub struct LocatedPolarity {
    slot: usize,
    value: Polarity,
}
