use chrono::{DateTime, Local, TimeZone, Utc};
use serde::{Deserialize, Serialize};

#[derive(Debug)]
pub struct Date {
    pub date: DateTime<Local>,
}

#[derive(Debug, Deserialize, Serialize)]
struct RawDate {
    #[serde(rename = "$date")]
    pub date: RawDateInner,
}

#[derive(Debug, Deserialize, Serialize)]
struct RawDateInner {
    #[serde(rename = "$numberLong")]
    pub number_long: String,
}

impl<'de> Deserialize<'de> for Date {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let r = RawDate::deserialize(deserializer)?;

        let timestamp: i64 = r.date.number_long.parse().unwrap();

        Ok(Date {
            date: Utc
                .timestamp_millis_opt(timestamp)
                .single()
                .unwrap()
                .with_timezone(&Local),
        })
    }
}

impl Serialize for Date {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        RawDate {
            date: RawDateInner {
                number_long: self.date.timestamp_millis().to_string(),
            },
        }
        .serialize(serializer)
    }
}
