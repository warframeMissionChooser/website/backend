{
  description = "Warframe Mission Chooser";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

    crane = {
      url = "github:ipetkov/crane";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    fenix = {
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = {
    self,
    nixpkgs,
    crane,
    fenix,
    flake-utils,
    ...
  }: let
    lib = nixpkgs.lib;
    systems = lib.lists.concatMap (s: [s "${s}-musl"]) flake-utils.lib.defaultSystems;
  in
    flake-utils.lib.eachSystem systems (raw_system: let
      system = lib.strings.removeSuffix "-musl" raw_system;
      musl = lib.strings.hasSuffix "-musl" raw_system;

      pkgs = nixpkgs.legacyPackages.${system};

      fenixToolchain = with fenix.packages.${system};
        if musl
        then
          combine [
            stable.defaultToolchain
            targets.x86_64-unknown-linux-musl.stable.rust-std
          ]
        else stable.defaultToolchain;

      craneLib = crane.lib.${system}.overrideToolchain fenixToolchain;

      commonArgs =
        {
          src = craneLib.cleanCargoSource ./.;
          pname = "warframe-web";

          doInstallCargoArtifacts = false;
          doCheck = false;

          buildInputs = with pkgs; [openssl];
          nativeBuildInputs = with pkgs; [pkg-config];
        }
        // lib.optionalAttrs musl {CARGO_BUILD_TARGET = "x86_64-unknown-linux-musl";};

      cargoArtifacts = craneLib.buildDepsOnly commonArgs // {doInstallCargoArtifacts = true;};
      fullArgs = commonArgs // {inherit cargoArtifacts;};

      warframe-web = craneLib.buildPackage fullArgs;
      doc = craneLib.cargoDoc (fullArgs
        // {
          installPhaseCommand = "cp -r target/doc $out";
        });
      test = craneLib.cargoNextest (fullArgs
        // {
          doCheck = true;

          cargoNextestExtraArgs = "--profile ci";
          buildPhaseCargoCommand = "cargo nextest --version";
          installPhaseCommand = "cp target/nextest/ci/junit.xml $out";
          extraSuffix = ".xml";

          # partitions = 1;
          # partitionType = "count";
        });
      container = pkgs.dockerTools.buildLayeredImage {
        name = "warframe-web";
        config = {
          Cmd = ["${warframe-web}/bin/warframe_web"];
          Env = [
            "SSL_CERT_FILE=${pkgs.cacert}/etc/ssl/certs/ca-bundle.crt"
            "PORT=8080"
          ];
          ExposedPorts = {
            "8080/tcp" = {};
          };
        };
      };
    in {
      checks = {
        inherit warframe-web doc test container;
        fmt = craneLib.cargoFmt commonArgs;
        clippy = craneLib.cargoClippy fullArgs // {cargoClippyExtraArgs = "--all-targets -- --deny warnings";};
      };

      packages = {
        default = warframe-web;
        inherit doc test container;
      };

      formatter = pkgs.alejandra;

      apps.default = flake-utils.lib.mkApp {
        drv = warframe-web;
        name = "warframe_web";
      };

      devShells.default = pkgs.mkShell {
        inputsFrom = [warframe-web doc test container];
        nativeBuildInputs = [
          fenix.packages.${system}.rust-analyzer
        ];
      };
    });
}
