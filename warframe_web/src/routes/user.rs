use std::{
    collections::{HashMap, HashSet},
    num::NonZeroU32,
};

use crate::{
    connection::UserInventory,
    data::{AppState, HExports},
    own_status::Status,
    response::{craft_info::CraftItemInfo, item::ItemInfo, Response},
    OwnStatus,
};
use axum::{
    extract::{FromRef, Path, Query, State},
    response::IntoResponse,
    routing::{get, post},
    Json, Router,
};
use serde::{Deserialize, Serialize};
use tracing::info;
use warframe_exports::{recipes::Recipe, Exports};
use warframe_inventory::{Date, Inventory, PendingRecipe};

pub(super) fn router<S>() -> Router<S>
where
    S: Clone + Send + Sync + 'static,
    HExports: FromRef<S>,
    AppState: FromRef<S>,
{
    Router::new()
        .route("/valid", get(valid))
        .route("/list", get(list))
        .route("/raw_data", get(raw_data))
        .route("/list_need_xp", get(get_list))
        .route("/craft_info", get(get_craft_info).post(craft_info))
        .route("/required_items", get(required_items))
        .route("/update/:mail", post(post_user_data))
}

async fn valid(_user: UserInventory) -> Json<bool> {
    // Validity checked by User guard
    Json(true)
}

async fn list(State(state): State<AppState>) -> impl IntoResponse {
    let state = state.user_data().user_list().await;
    Json(state)
}

async fn raw_data(user: UserInventory) -> impl IntoResponse {
    Json(user.inventory()).into_response()
}

async fn get_list(user: UserInventory, exports: State<HExports>) -> impl IntoResponse {
    let inv = user.inventory();

    let res: Vec<ItemInfo<Status>> = exports
        .xp_items()
        .map(|item| {
            ItemInfo::new(
                item.as_item(),
                OwnStatus::new(item, inv, &exports).status(),
                &exports,
            )
        })
        .collect();

    Response::new(res)
}

#[derive(Deserialize)]
struct Piece {
    piece: Option<String>,
}

async fn get_craft_info(
    user: UserInventory,
    exports: State<HExports>,
    Query(Piece { piece }): Query<Piece>,
) -> impl IntoResponse {
    let pieces: Vec<String> = match piece {
        Some(p) => {
            vec![p]
        }
        None => {
            let inventory = user.inventory();

            exports
                .xp_items()
                .flat_map(|item| {
                    let craft_required = OwnStatus::new(item, inventory, &exports).craft_required();

                    if craft_required {
                        Some(item.as_item().unique_name().to_owned())
                    } else {
                        None
                    }
                })
                .collect()
        }
    };

    craft_info(user, exports, Json(pieces)).await
}

async fn craft_info(
    user: UserInventory,
    exports: State<HExports>,
    data: Json<Vec<String>>,
) -> impl IntoResponse {
    let inventory = user.inventory();

    let craft_info: Vec<CraftItemInfo> = data
        .iter()
        .map(|name| CraftItemInfo::new(name, 1, &exports, inventory))
        .collect();

    Response::new(craft_info).into_response()
}

async fn required_items(
    query: Query<HashMap<String, String>>,
    user: UserInventory,
    exports: State<HExports>,
) -> impl IntoResponse {
    let inventory = user.inventory();

    #[derive(Default, Serialize)]
    struct Info<'a> {
        required: u32,                              // Total num required
        crafted: u32,                               // Amount crafted
        craft_source: HashMap<&'a str, (u32, u32)>, // Recipe used to craft this item
        owned: u32,                                 // Amount already owned

        num_foundry: u32, // Number of iterm in foudry (cached)
        foundry: Vec<(&'a Recipe, &'a PendingRecipe)>, // Amount in foundry

        available_recipe: HashSet<&'a str>, // Recipe that lead to this item, if multiple options, select one with exclude_list
    }

    impl<'a> Info<'a> {
        fn new(name: &'a str, inventory: &'a Inventory, exports: &'a Exports) -> Self {
            let foundry = exports
                .recipe_for(name)
                .filter_map(|r| inventory.has_building(&r.unique_name).map(|p| (r, p)))
                .collect::<Vec<_>>();

            Self {
                required: 0,
                crafted: 0,
                craft_source: HashMap::new(),
                owned: inventory.num_item(name).map_or(0, u32::from),
                num_foundry: foundry.iter().map(|(r, _)| r.num).sum(),
                foundry,
                available_recipe: HashSet::default(),
            }
        }
        fn one(name: &'a str, inventory: &'a Inventory, exports: &'a Exports) -> Self {
            let mut s = Self::new(name, inventory, exports);
            s.required = 1;
            s
        }
        fn num_craft_required(&self) -> Option<NonZeroU32> {
            self.required
                .checked_sub(self.crafted + self.owned + self.num_foundry)
                .and_then(NonZeroU32::new)
        }
        fn used_by(&mut self, recipe: &'a Recipe, num: u32, ingredient_count: u32) {
            self.required += num * ingredient_count;
            let val = self.craft_source.entry(&recipe.unique_name).or_default();
            val.0 += num;
            val.1 += num * ingredient_count;
        }
    }

    let mut item_qte = exports
        .xp_items()
        .filter(|item| OwnStatus::new(*item, inventory, &exports).craft_required())
        .map(|item| {
            (
                item.unique_name(),
                Info::one(item.unique_name(), inventory, &exports),
            )
        })
        .collect::<HashMap<_, _>>();

    loop {
        let mut to_add = Vec::<_>::new();

        for (name, qte) in &mut item_qte {
            if let Some(missing) = qte.num_craft_required() {
                let recipes = exports.recipe_for(name).collect::<Vec<_>>();
                if let Some(r) = recipes
                    .iter()
                    .cloned()
                    .find(|r| query.get(&r.unique_name).is_none())
                {
                    let num_recipe = ((u32::from(missing) as f32) / (r.num as f32)).ceil() as u32;
                    assert!(num_recipe * r.num >= missing.into());

                    qte.crafted += num_recipe * r.num;
                    to_add.push((r, num_recipe));
                }

                qte.available_recipe
                    .extend(recipes.iter().map(|r| r.unique_name.as_str()));
            }
        }

        if to_add.is_empty() {
            break;
        }

        for (recipe, qte) in to_add {
            for ingredient in recipe.ingredients.iter().chain(&recipe.secret_ingredients) {
                item_qte
                    .entry(&ingredient.item_type)
                    .or_insert_with_key(|k| Info::new(k, inventory, &exports))
                    .used_by(recipe, qte, ingredient.item_count);
            }
        }
    }

    Json(
        item_qte
            .into_iter()
            .map(|(name, data)| {
                ItemInfo::from_name(name, data, &exports)
                    .unwrap_or_else(|| panic!("{name} not found"))
            })
            .collect::<Vec<_>>(),
    )
    .into_response()
}

// HACK: security: everyone can remove others data
async fn post_user_data(
    mail: Path<String>,
    state: State<AppState>,
    inventory: Json<Inventory>,
) -> Json<bool> {
    let status = state
        .user_data()
        .add_user(mail.to_string(), inventory.0)
        .await;

    info!(?status, mail = mail.as_str(), "Inventory saved");

    Json(true)
}
