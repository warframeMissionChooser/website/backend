// use crate::{data::exports::StateExports, response::item::ItemInfo};
use axum::{
    extract::{FromRef, Path, State},
    http::StatusCode,
    response::IntoResponse,
    routing::get,
    Json, Router,
};
use tracing::log::warn;

use crate::{data::HExports, response::item::ItemInfo};

pub(super) fn router<S>() -> Router<S>
where
    S: Send + Sync + Clone + 'static,
    HExports: FromRef<S>,
{
    Router::new().route("/item/*item_name", get(item_info))
}

async fn item_info(item_name: Path<String>, exports: State<HExports>) -> impl IntoResponse {
    let run = || {
        let item_name = format!("/{}", item_name.as_str());
        let item = exports
            .items()
            .find(|item| item.unique_name() == item_name)?;

        Some(Json(ItemInfo::new(item, (), &exports)))
    };

    let o = run();
    if o.is_none() {
        warn!("ERROR: Unable to find details for {item_name:?}")
    }
    o.ok_or(StatusCode::NOT_FOUND)
}
