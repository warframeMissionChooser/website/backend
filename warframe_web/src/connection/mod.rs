use crate::data::{user_data::InventoryHandle, AppState};
use axum::{
    extract::{FromRef, FromRequestParts, Query},
    http::{request::Parts, StatusCode},
    RequestPartsExt,
};
use serde::Deserialize;
use warframe_inventory::Inventory;

#[derive(Deserialize)]
struct UserQuery {
    mail: String,
}

pub struct UserInventory {
    mail: String,
    inventory: InventoryHandle,
}

impl UserInventory {
    pub fn mail(&self) -> &str {
        &self.mail
    }
    pub fn inventory(&self) -> &Inventory {
        &self.inventory
    }
}

#[axum::async_trait]
impl<S> FromRequestParts<S> for UserInventory
where
    S: Sync + Send,
    AppState: FromRef<S>,
{
    type Rejection = StatusCode;

    async fn from_request_parts(parts: &mut Parts, state: &S) -> Result<Self, Self::Rejection> {
        let mail: Query<UserQuery> = parts
            .extract()
            .await
            .map_err(|_err| StatusCode::UNAUTHORIZED)?;

        let state = AppState::from_ref(state);
        let inventory = state
            .user_data()
            .read_user(&mail.mail)
            .await
            .ok_or(StatusCode::NOT_FOUND)?;

        Ok(Self {
            mail: mail.0.mail,
            inventory,
        })
    }
}
