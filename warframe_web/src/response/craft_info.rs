use super::Response;
use serde::Serialize;

use warframe_exports::{recipes::Recipe, relic_arcanes::RelicArcane, Exports};
use warframe_inventory::Inventory;

pub type ItemInfoList<'a> = Response<Vec<CraftItemInfo<'a>>>;

#[derive(Debug, Serialize)]
pub struct CraftItemInfo<'a> {
    pub craft_item: &'a str,
    pub count: u32,

    pub status: Status<'a>,
}

#[derive(Debug, Serialize)]
pub struct CraftInfo<'a> {
    pub name: &'a str,

    pub ingredients: Vec<CraftItemInfo<'a>>,
}

#[derive(Debug, Serialize)]
pub struct RelicInfo<'a> {
    pub item: &'a str,

    pub rarity: &'a str,
}

#[derive(Debug, Serialize)]
pub enum Status<'a> {
    Owned,
    CraftRequired(Vec<CraftInfo<'a>>),
    RelicRequired(Vec<RelicInfo<'a>>),
    NotFound,
}

impl<'a> CraftItemInfo<'a> {
    pub fn new(name: &'a str, count: u32, exports: &'a Exports, inventory: &Inventory) -> Self {
        let status = if inventory.has_enough_item(name, count) {
            Status::Owned
        } else {
            let crafts: Vec<_> = exports
                .recipe_for(name)
                .map(|recipe| CraftInfo::new(recipe, exports, inventory))
                .collect();

            let relics: Vec<_> = exports
                .relic_for(name)
                .map(|relic| RelicInfo::new(relic, exports, inventory, name))
                .collect();

            match (crafts.is_empty(), relics.is_empty()) {
                (false, false) => panic!("..."),
                (false, true) => Status::CraftRequired(crafts),
                (true, false) => Status::RelicRequired(relics),
                (true, true) => Status::NotFound,
            }
        };

        Self {
            craft_item: name,
            count,
            status,
        }
    }
}

impl<'a> CraftInfo<'a> {
    pub fn new(recipe: &'a Recipe, exports: &'a Exports, inventory: &Inventory) -> Self {
        let ingredients = std::iter::once((&recipe.unique_name, 1))
            .chain(
                recipe
                    .ingredients
                    .iter()
                    .map(|ing| (&ing.item_type, ing.item_count)),
            )
            .map(|(name, count)| CraftItemInfo::new(name, count, exports, inventory))
            .collect();

        Self {
            name: &recipe.unique_name,
            ingredients,
        }
    }
}

impl<'a> RelicInfo<'a> {
    pub fn new(
        relic: &'a RelicArcane,
        _exports: &Exports,
        _inventory: &Inventory,
        name: &str,
    ) -> Self {
        RelicInfo {
            item: &relic.name,

            // rarity: relic.relic_rewards.iter().find(|e| e.reward_name),
            rarity: &relic
                .relic_rewards
                .iter()
                .find(|reward| reward.reward_name.replace("/StoreItems", "") == name)
                .unwrap()
                .rarity,
        }
    }
}
