/*
use super::Response;
use serde::Serialize;

use warframe_exports::{
    recipes::{Ingredients as RecipeIngredients, Recipe},
    relic_arcanes::RelicArcane,
    Exports,
};
use warframe_inventory::Inventory;

pub type ItemInfoList = Response<Vec<CraftItemInfo>>;

#[derive(Debug, Serialize)]
pub struct CraftItemInfo {
    pub craft_item: String,
    pub count: u32,

    pub status: Status,
}

#[derive(Debug, Serialize)]
pub struct CraftInfo {
    pub name: String,

    pub ingredients: Vec<CraftItemInfo>,
}

#[derive(Debug, Serialize)]
pub struct RelicInfo {
    pub item: String,

    pub rarity: String,
}

#[derive(Debug, Serialize)]
pub enum Status {
    Owned,
    CraftRequired(Vec<CraftInfo>),
    RelicRequired(Vec<RelicInfo>),
    // crafts: Vec<CraftInfo>,
    // relics: Vec<RelicInfo>,
    NotFound,
}

impl CraftItemInfo {
    pub fn new(
        name: &str,
        count: u32,
        exports: &Exports,
        inventory: &Inventory
    ) -> Self {
        // image_url: format!("https://content.warframe.com/PublicExport/{}", exports.get_image(item).unwrap()),

            let status = if inventory.has_enough_item(&name, count) {
                Status::Owned
            } else {
                let crafts: Vec<_> = exports.recipe_for(name).map(|recipe| CraftInfo::new(recipe, exports, inventory)).collect();
                let relics: Vec<_> = exports.relic_for(name).map(|relic| RelicInfo::new(relic, exports, inventory)).collect();

                match (crafts.is_empty(), relics.is_empty()) {
                    (false, false) => panic!("..."),
                    (false, true) => Status::CraftRequired(crafts),
                    (true, false) => Status::RelicRequired(relics),
                    (true, true) => Status::NotFound,
                }
            };


        Self {
            craft_item: name.to_string(),
            count,
            status,
        }
    }
}

impl CraftInfo {
    pub fn new(
        recipe: &Recipe,
        exports: &Exports,
        inventory: &Inventory
    ) -> Self {
        let ingredients = std::iter::once(&RecipeIngredients {
            item_type: recipe.unique_name.to_string(),
            item_count: 1,
            product_category: None,
        })
        .chain(recipe.ingredients.iter())
        .map(|ingredient| {
            CraftItemInfo::new(
                &ingredient.item_type.to_string(),
                ingredient.item_count,
                exports,
                inventory
            )
        })
        .collect();

        Self {
            name: recipe.unique_name.to_string(),
            ingredients,
        }
    }
}

impl RelicInfo {
    pub fn new(
        relic: &RelicArcane,
        exports: &Exports,
        inventory: &Inventory
    ) -> Self {
        RelicInfo {
            item: relic.name.to_string(),

            rarity: "WIP".to_string(),
        }
    }
}
*/
