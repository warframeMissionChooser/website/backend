use super::Exports;
use std::borrow::Cow;

pub trait Item: Send + Sync {
    fn unique_name(&self) -> &str;

    fn name(&self) -> Cow<str>;
    fn description(&self) -> Cow<str>;
    fn kind(&self) -> ItemKind;
}

#[derive(Debug, serde::Serialize)]
pub enum ItemKind {
    Warframe,
    Sentinel,
    SentinelWeapon,
    PrimaryWeapon,
    SecondaryWeapon,
    MeleeWeapon,

    SpecialWeapon,

    Other,
}

pub trait XPItem: Item {
    fn as_item(&self) -> &dyn Item;

    fn max_level(&self) -> u8 {
        30
    }

    fn xp_multiplier(&self) -> u32 {
        1
    }

    fn level(&self, xp: u32) -> u8 {
        let xp = xp * self.xp_multiplier();

        (0..=self.max_level())
            .rev()
            .find(|level| level_to_xp(*level) <= xp)
            .unwrap_or(0)
    }

    fn is_done(&self, xp: u32) -> bool {
        self.level(xp) == self.max_level()
    }

    fn unique(&self) -> (Cow<str>, UniqueLevel) {
        let name = self.name();

        if let Some(n) = MK1_ITEMS.handle(&name) {
            (n, UniqueLevel::Mk1)
        } else if let Some(n) = IMPROVED_ITEMS
            .iter()
            .find_map(|variant| variant.handle(&name))
        {
            (n, UniqueLevel::Improved)
        } else {
            (name, UniqueLevel::Normal)
        }
    }

    fn get_prime<'a>(&self, exports: &'a Exports) -> Vec<&'a dyn XPItem> {
        let (name, ty) = self.unique();
        exports
            .xp_items() // todo!(performance cache table)
            .into_iter()
            .filter(move |w| {
                let (name2, ty2) = w.unique();
                self.unique_name() != w.unique_name() && name2 == name && ty < ty2
            })
            .collect()
    }
}

fn level_to_xp(level: u8) -> u32 {
    let level: u32 = level.into();

    1000 * level.pow(2)
}

#[derive(Debug, PartialOrd, PartialEq)]
pub enum UniqueLevel {
    Mk1,
    Normal,
    Improved,
}

enum ItemVariant {
    Prefix(&'static str),
    Suffix(&'static str),
}

impl ItemVariant {
    #[allow(clippy::ptr_arg)] // Allow &Cow here, plain &str also provided
    fn handle<'a>(&self, name: &Cow<'a, str>) -> Option<Cow<'a, str>> {
        match name {
            Cow::Borrowed(name) => self.handle_str(name).map(Cow::Borrowed),
            Cow::Owned(name) => self.handle_str(name).map(String::from).map(Cow::Owned),
        }
    }

    fn handle_str<'a>(&self, name: &'a str) -> Option<&'a str> {
        match self {
            ItemVariant::Prefix(prefix) => name.strip_prefix(prefix),
            ItemVariant::Suffix(suffix) => name.strip_suffix(suffix),
        }
        .map(str::trim)
    }
}

const MK1_ITEMS: ItemVariant = ItemVariant::Prefix("MK1-");
const IMPROVED_ITEMS: &[ItemVariant] = &[
    ItemVariant::Suffix("PRIME"),
    ItemVariant::Prefix("RAKTA"),
    ItemVariant::Suffix("UMBRA"),
    ItemVariant::Prefix("DEX"),
];
