use serde::{Deserialize, Serialize};

pub type Customs = Vec<Custom>;

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Custom {
    pub unique_name: String,
    pub name: String,

    pub description: Option<String>,

    pub codex_secret: bool,
    #[serde(default)]
    pub exclude_from_codex: bool,
}
