use crate::utils::Rarity;
use serde::{Deserialize, Serialize};

pub type RelicArcanes = Vec<RelicArcane>;

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct RelicArcane {
    pub unique_name: String,
    pub name: String,
    pub description: Option<String>,

    pub codex_secret: bool,
    #[serde(default)]
    pub exclude_from_codex: bool,
    pub rarity: Option<Rarity>,

    #[serde(default)]
    pub relic_rewards: Vec<RelicReward>,

    #[serde(default)]
    pub level_stats: Vec<LevelStat>,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct RelicReward {
    pub reward_name: String,
    pub rarity: String,
    pub tier: u8,
    pub item_count: u16,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct LevelStat {
    pub stats: Vec<String>,
}
