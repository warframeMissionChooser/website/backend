use serde::{Deserialize, Serialize};

pub type Manifest = Vec<Entry>;

#[derive(Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
#[serde(rename_all = "camelCase")]
pub struct Entry {
    pub unique_name: String,

    pub texture_location: String,
}
