use crate::Item;
use serde::{Deserialize, Serialize};
use std::borrow::Cow;

pub type Resources = Vec<Resource>;

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Resource {
    pub unique_name: String,
    pub name: String,
    pub parent_name: String,
    pub description: String,
    pub long_description: Option<String>,

    pub codex_secret: bool,
    #[serde(default)]
    pub exclude_from_codex: bool,
    #[serde(default = "bool_true")]
    pub show_in_inventory: bool,

    pub prime_selling_price: Option<u32>,
}

fn bool_true() -> bool {
    true
}

impl Item for Resource {
    fn unique_name(&self) -> &str {
        &self.unique_name
    }

    fn name(&self) -> Cow<str> {
        self.name.as_str().into()
    }

    fn description(&self) -> Cow<str> {
        self.description.as_str().into()
    }

    fn kind(&self) -> crate::ItemKind {
        crate::ItemKind::Other
    }
}
