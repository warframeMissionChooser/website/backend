use crate::{Item, ItemKind, XPItem};
use serde::{Deserialize, Serialize};
use std::borrow::Cow;

pub type Weapons = Vec<Weapon>;

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
//#[serde(deny_unknown_fields)]
pub struct Weapon {
    pub name: String,
    pub unique_name: String,
    pub description: String,

    pub codex_secret: bool,
    #[serde(default)]
    pub exclude_from_codex: bool,
    #[serde(default = "default_level")]
    pub max_level_cap: u8,

    pub damage_per_shot: [f32; 20],
    pub total_damage: f32,

    pub critical_chance: f32,
    pub critical_multiplier: f32,
    pub proc_chance: f32,
    pub fire_rate: f32,

    // pub mastery_req: u8,
    #[serde(flatten)]
    pub product_category: Category,

    pub omega_attenuation: f32,

    pub slot: Option<i64>,

    pub prime_omega_attenuation: Option<f64>,

    #[serde(default)]
    sentinel: bool,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(tag = "productCategory")]
pub enum Category {
    Melee,           //(MeleeData),
    Pistols,         //(PistolsData),
    LongGuns,        //(PistolsData),
    SpaceGuns,       //(PistolsData),
    SpaceMelee,      //(MeleeData),
    SentinelWeapons, //(PistolsData),
    SpecialItems,
    OperatorAmps,
}
/*
#[derive(Debug, Deserialize)]
pub struct MeleeData {
    pub blocking_angle: Option<i64>,
    pub combo_duration: Option<i64>,
    pub follow_through: Option<f64>,
    pub heavy_attack_damage: Option<i64>,
    pub heavy_slam_attack: Option<i64>,
    pub heavy_slam_radial_damage: Option<i64>,
    pub heavy_slam_radius: Option<i64>,
    pub range: Option<f64>,
    pub slam_attack: Option<i64>,
    pub slam_radial_damage: Option<i64>,
    pub slam_radius: Option<i64>,
    pub slide_attack: Option<i64>,
    pub wind_up: Option<f64>,
}

#[derive(Debug, Deserialize)]
pub struct PistolsData {
    pub accuracy: f32,
    pub magazine_size: u8,
    pub multishot: u8,
    pub noise: Noise,
    pub reload_time: Option<f64>,
    pub trigger: Trigger,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "UPPERCASE")]
pub enum Noise {
    #[serde(alias = "BRUYANT")]
    Alarming,

    #[serde(alias = "SILENCIEUX")]
    Silent,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "UPPERCASE")]
pub enum Trigger {
    // Same in french
    Auto,

    // Same in french
    Semi,

    #[serde(alias = "RAFALE")]
    Burst,

    #[serde(alias = "CONTINUE")]
    Held,

    // Same in french
    Duplex,

    #[serde(alias = "CHARGÉE")]
    Charge,

    #[serde(alias = "ACTIVÉES")]
    Active,

    #[serde(rename = "AUTO BURST")]
    #[serde(alias = "RAFALE AUTO")]
    AutoBurst,
}
*/
/*
fn path<'de, D>(deserializer: D) -> Result<u8, D::Error>
where D: serde::Deserializer<'de> {
    struct V;
    impl<'de> serde::de::Visitor<'de> for V {
        type Value = u8;

        fn expecting(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
            write!(f, "mastery_req field")
        }

        fn visit_map<A: serde::de::MapAccess<'de>>(self, mut map_access: A) -> Result<Self::Value, A::Error> {
            let mut res = None;
            while let Some((k, v)) = map_access.next_entry::<&str, u8>()? {
                if k != "masteryReq" { continue; }

                match res {
                    Some(val) => assert_eq!(v, val),
                    None => res = Some(v),
                }
            }
            Ok(res.unwrap())
        }
    }
    deserializer.deserialize_map(V)
}
*/

// fn multishot() -> u8 { 1 }
fn default_level() -> u8 {
    30
}

impl Item for Weapon {
    fn unique_name(&self) -> &str {
        &self.unique_name
    }

    fn name(&self) -> Cow<str> {
        self.name.as_str().into()
    }

    fn description(&self) -> Cow<str> {
        self.description.as_str().into()
    }

    fn kind(&self) -> ItemKind {
        match self.product_category {
            Category::Melee | Category::SpaceMelee => ItemKind::MeleeWeapon,
            Category::LongGuns | Category::SpaceGuns => ItemKind::PrimaryWeapon,
            Category::Pistols => ItemKind::SecondaryWeapon,
            Category::SentinelWeapons => ItemKind::SentinelWeapon,
            Category::SpecialItems => ItemKind::SentinelWeapon,
            Category::OperatorAmps => ItemKind::SpecialWeapon,
        }
    }
}

impl XPItem for Weapon {
    fn as_item(&self) -> &dyn Item {
        self
    }

    fn max_level(&self) -> u8 {
        self.max_level_cap
    }

    fn xp_multiplier(&self) -> u32 {
        2
    }
}
