use crate::{Item, ItemKind, XPItem};
use serde::{Deserialize, Serialize};
use std::borrow::Cow;

pub type Warframes = Vec<Warframe>;

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
//#[serde(deny_unknown_fields)]
pub struct Warframe {
    pub unique_name: String,
    pub name: String,
    pub parent_name: String,

    pub description: String,
    pub passive_description: Option<String>,

    pub health: i16,
    pub shield: i16,
    pub armor: i16,
    pub stamina: i16,
    pub power: i16,

    pub codex_secret: bool,
    pub mastery_req: i16,

    pub sprint_speed: f32,

    pub abilities: Vec<Ability>,
    pub product_category: String,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Ability {
    pub ability_unique_name: String,
    pub ability_name: String,
    pub description: String,
}

impl Item for Warframe {
    fn unique_name(&self) -> &str {
        &self.unique_name
    }

    fn name(&self) -> Cow<str> {
        self.name.as_str().into()
    }

    fn description(&self) -> Cow<str> {
        self.description.as_str().into()
    }

    fn kind(&self) -> ItemKind {
        ItemKind::Warframe
    }
}

impl XPItem for Warframe {
    fn as_item(&self) -> &dyn Item {
        self
    }
}
