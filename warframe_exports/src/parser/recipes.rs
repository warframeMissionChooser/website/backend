use crate::{Item, ItemKind};
use serde::{Deserialize, Serialize};
use std::borrow::Cow;

pub type Recipes = Vec<Recipe>;

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
#[serde(deny_unknown_fields)]
pub struct Recipe {
    pub unique_name: String,
    pub result_type: String,

    pub build_price: u32,
    pub build_time: u32,
    pub skip_build_time_price: u32,

    pub consume_on_use: bool,

    pub num: u32,

    pub codex_secret: bool,

    #[serde(default)]
    pub exclude_from_codex: bool,
    pub prime_selling_price: Option<u32>,
    #[serde(default)]
    pub always_available: bool,

    pub ingredients: Vec<Ingredients>,
    pub secret_ingredients: Vec<Ingredients>,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "PascalCase")]
pub struct Ingredients {
    pub item_type: String,
    pub item_count: u32,
    pub product_category: Option<String>,
}

impl Item for Recipe {
    fn unique_name(&self) -> &str {
        &self.unique_name
    }

    fn name(&self) -> Cow<str> {
        if self.num == 1 {
            format!("Recipe for {}", self.result_type)
        } else {
            format!("Recipe for {} x {}", self.num, self.result_type)
        }
        .into()
    }

    fn description(&self) -> Cow<str> {
        format!(
            "{} credits + {}s or {} platinum. {}",
            self.build_price,
            self.build_time,
            self.skip_build_time_price,
            if self.consume_on_use {
                "Consume on use."
            } else {
                ""
            }
        )
        .into()
    }

    fn kind(&self) -> ItemKind {
        ItemKind::Other
    }
}
