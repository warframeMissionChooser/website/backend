use crate::{ETagInfo, Exports};

use reqwest::Client;

use std::convert::TryInto;

mod file_list;
use file_list::{FileList, Language};

mod export_slice;
pub use export_slice::ExportSlice;

pub(crate) struct ExportsInfo {
    pub(crate) export: Exports,
    pub(crate) etag: ETagInfo,
}

impl ExportsInfo {
    pub async fn fetch(etag: Option<&ETagInfo>) -> reqwest::Result<Option<Self>> {
        let client = Client::new();

        let file = FileList::new(&client, Language::En, etag.map(|e| e.etag.as_str())).await?;

        if let Some(file) = file {
            let aggregated: ExportSlice = file.download_all(&client).await?.into_iter().sum();
            let export: Option<Exports> = aggregated.try_into().ok();

            Ok(export.map(|export| ExportsInfo {
                export,
                etag: file.etag,
            }))
        } else {
            Ok(None)
        }
    }
}
