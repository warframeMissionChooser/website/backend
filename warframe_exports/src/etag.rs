use chrono::{DateTime, Utc};
use reqwest::header::{HeaderMap, ETAG, LAST_MODIFIED};
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct ETagInfo {
    pub date: DateTime<Utc>,
    pub etag: String,
}

use std::convert::TryFrom;

impl TryFrom<&HeaderMap> for ETagInfo {
    type Error = &'static str;

    fn try_from(header: &HeaderMap) -> Result<Self, Self::Error> {
        let etag = header
            .get(ETAG)
            .ok_or("An etag value should be provided")?
            .to_str()
            .or(Err("ETag sould be string representable"))?
            .trim_start_matches('"')
            .trim_end_matches('"')
            .to_string();

        let date = DateTime::parse_from_rfc2822(
            header
                .get(LAST_MODIFIED)
                .ok_or("An etag value should be provided")?
                .to_str()
                .or(Err("ETag sould be string representable"))?,
        )
        .or(Err("Invalid provided date"))?
        .into();

        Ok(Self { date, etag })
    }
}
